---
title: "Analyse de la corrélation entre la croissance économique et le mix énergétique"
author: "BADZI Mohamed, FATHALLAH Mariam, HOUNKARA Abdelkoddous, LIU Xiaoman"
date: "Mars 2024"
output:
  html_document: default
  word_document: default
  pdf_document:
    number_sections: yes
geometry: margin=1.5in, top=0.5in, bottom=0.5in
subtitle: Une démonstration RStudio et une ébauche d'analyse statistique
---

\newpage


# Table des matières
1. [Introduction](#introduction)
   - [Contexte / Description des données](#context-dataset-description)
2. [Méthodologie](#methodologie)
   - [Positionnement du problème](#positionnement-du-problème)
   - [Formulation de la question](#formulation-de-la-question)
   - [Procédures de nettoyage des données](#procédures-de-nettoyage-des-données)
   - [Flux de travail scientifique](#flux-de-travail-scientifique)
   - [Choix de représentation des données](#choix-de-représentation-des-données)
3. [Analyse en programmation littéraire](#analyse-en-programmation-littéraire)
4. [Synthèse des Résultats](#synthèse-des-résultats)
5. [Références](#references)

\newpage

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction
Pour ce projet, nous allons examiner la corrélation entre la croissance économique d'un pays et son mix énergétique. Cette corrélation est intéressante car elle peut nous aider à comprendre dans quelle mesure les choix énergétiques d'un pays influent sur sa performance économique, et vice versa.

## Context / Dataset Description
### Données de consommation d'énergie (df_filtered_tidy)
L'ensemble de données sur la consommation d'énergie contient des informations sur la consommation de différents types d'énergie (tels que solaire, éolien, hydro, gaz, charbon, pétrole, etc.) pour divers pays au fil des années.

#### Variables 
Entité : Nom du pays.
Code : Code du pays.
Année : Année de l'observation.
Type d'énergie : Type d'énergie.
Consommation : Valeur de la consommation d'énergie.

### Données du PIB (ppa_data_tidy)
L'ensemble de données du PIB comprend des données sur le PIB (Produit Intérieur Brut) mesuré en dollars internationaux courants (PPA - Parité de Pouvoir d'Achat) pour différents pays sur plusieurs années.

#### Variables 
Nom du pays : Nom du pays.
Code du pays : Code du pays.
Nom de l'indicateur : Nom de l'indicateur (PIB).
Code de l'indicateur : Code de l'indicateur.
Année : Année de l'observation.
PIB_PPP : Valeur du PIB en dollars internationaux courants (PPA).


\newpage

# Methodologie

## Positionnement du problème
La transition énergétique et la croissance économique sont deux éléments clés du développement durable. Comprendre la relation entre le mix énergétique d'un pays — c'est-à-dire la répartition de différentes sources d'énergie utilisées pour la production d'énergie — et sa croissance économique est essentiel pour élaborer des politiques qui favorisent à la fois la durabilité environnementale et la prospérité économique.

### Contexte Économique
Le mix énergétique d'un pays peut avoir un impact significatif sur sa performance économique. Les différentes sources d'énergie ont des coûts associés variables, des implications en termes de sécurité énergétique et des effets sur l'environnement. Par conséquent, la composition du mix énergétique peut influencer la compétitivité économique d'un pays, sa capacité à attirer des investissements et sa résilience face aux chocs énergétiques.

### Contexte Environnemental
Les choix énergétiques des pays ont également des conséquences sur l'environnement, notamment en ce qui concerne les émissions de gaz à effet de serre et le changement climatique. Les pays qui dépendent fortement des combustibles fossiles peuvent avoir des niveaux d'émissions plus élevés, ce qui peut avoir des implications pour la santé publique et l'écosystème mondial. La transition vers des sources d'énergie plus propres et renouvelables est devenue une priorité pour de nombreux pays en raison de ces préoccupations environnementales.

## Formulation de la question
En général, la croissance économique s'accompagne d'une augmentation de la consommation d'énergie. Avec l'augmentation des activités économiques, la demande en énergie des secteurs industriels, des transports, du commerce et des résidents augmente également. Cela est dû au fait que la production de plus de biens et services nécessite généralement plus d'entrées énergétiques. 
Bien que l'augmentation de la consommation d'énergie soit souvent le résultat de la croissance économique, une offre énergétique suffisante est également un moteur important de la croissance économique. Sans un approvisionnement énergétique stable, la production industrielle, les transports et d'autres activités économiques seraient limités, affectant ainsi la performance économique globale.
Avec les progrès technologiques et l'amélioration de l'efficacité énergétique, la relation entre croissance économique et consommation d'énergie peut changer. Par exemple, en améliorant l'efficacité énergétique et en adoptant des technologies énergétiques plus propres, il est possible de réaliser la croissance économique tout en réduisant la dépendance à l'énergie et en diminuant l'impact environnemental.
C'est pourquoi nous avons posé la question - quelle est la corrélation entre la croissance économique nationale et la consommation totale d'énergie ?

### Pourquoi le PPA ?
Alors, pourquoi utiliser le PIB ajusté en fonction de la parité de pouvoir d'achat (PPA) comme source de données représentant la croissance économique ?
Le PIB ajusté en fonction de la PPA reflète mieux l'activité économique réelle et le pouvoir d'achat d'un pays, plutôt que de simples valeurs converties par les taux de change. Cela est particulièrement important pour comparer et analyser les relations entre l'économie et la consommation d'énergie entre différents pays, car les différences de niveau des prix et de coût de vie entre les pays peuvent déformer les données du PIB non ajustées.
 Pourquoi choisir la consommation totale d'énergie comme source de données ?
Choisir la consommation totale d'énergie annuelle comme indicateur permet de refléter de manière exhaustive la situation de l'utilisation de l'énergie dans un pays sur une période donnée. Cela inclut tous les types de consommation d'énergie, tels que le pétrole, le gaz naturel, le charbon et les énergies renouvelables, offrant ainsi une perspective globale pour étudier la relation entre l'activité économique et la demande énergétique.


### Qu'est-ce que l'PPA ?
La parité de pouvoir d'achat (PPA) est une théorie et une méthode de calcul économiques utilisées pour déterminer la valeur relative des monnaies de différents pays. L'idée centrale de la PPP est que, en l'absence de barrières commerciales et de coûts de transaction, le prix d'un même bien ou service devrait être égal dans différents pays, une fois pris en compte le taux de change. Autrement dit, une unité de monnaie devrait pouvoir acheter la même quantité de biens et services dans n'importe quel pays.


## Procédures de nettoyage des données

### Libraries 
```{r, warning=FALSE}
library(tidyverse)
library(dplyr)
```

### Selection du pays
ici on as remarque que la selection par code était beaucoup plus pertinante que la selection par nom de pays car le nom de pays est representé de manière differente dans les differents datasets
```{r, warning=FALSE}
#pays_selected <- "Mexico"
code_selected <- "MEX"
```
### Récuperation + traitement des données mix energique
```{r, warning=FALSE}
# Charger les données dans un dataframe
df <- read_csv("Datasets/Data/Cleaned_energy-consumption-by-source-and-country.csv")
```
### Standarilisation des données (tidy)
```{r, warning=FALSE}
df_tidy <- df %>%
  pivot_longer(
    cols = `Other renewables (including geothermal and biomass) - TWh`:`Oil consumption - TWh`,
    names_to = "Type d'énergie",
    values_to = "Consommation",
    values_drop_na = TRUE # Optionnel, pour supprimer les rangées avec des valeurs NA dans 'Consommation'
  ) %>%
  mutate(
    `Type d'énergie` = str_replace(`Type d'énergie`, " - TWh", "") # Optionnel, pour nettoyer les noms des types d'énergie
  )

df_filtered_tidy <- df_tidy %>%
  filter(!is.na(Code) & Code != "")

df_filtered_tidy_pays <- df_filtered_tidy %>%
  #filter(Entity == pays_selected)
  filter(Code == code_selected)
```
###  Récuperation + traitement des données ppa
```{r, warning=FALSE}
ppa_data <- read_csv("Datasets/Data/Cleaned_API_NY.GDP.MKTP.PP.CD_DS2_fr_csv_v2_12982.csv")

ppa_data_tidy <- ppa_data %>%
  pivot_longer(
    cols = `1990`:`2022`, 
    names_to = "Year", 
    values_to = "GDP_PPP"
  )

# Filtre pour le pays choisi
df_ppa_pays <- ppa_data_tidy %>%
  #filter(`Country Name` == pays_selected)
  filter(`Country Code` == code_selected)
```



## Flux de travail scientifique


## Choix de représentation des données

Nous avons opté pour une transformation longue des données afin de faciliter les analyses et les visualisations. La consommation d'énergie a été extraite et pivotée pour un traitement plus aisé. Dans le cadre de notre analyse, nous avons également calculé la croissance annuelle du PPA, ce qui permet d'évaluer les changements relatifs d'une année sur l'autre.

```{r, warning=FALSE}

df_ppa_pays_pourcentage <- df_ppa_pays %>%
  arrange(Year) %>% # Assurez-vous que les données sont dans l'ordre chronologique
  mutate(
    Growth = ((GDP_PPP / lag(GDP_PPP)) - 1) * 100 # Calcule le pourcentage de croissance annuelle
  )

# Calculer la somme totale de la consommation d'énergie par année pour la France
df_sum_energy_pays <- df_filtered_tidy_pays %>%
   group_by(Year) %>%
   summarise(Total_Consumption = sum(Consommation)) %>%
   ungroup()
```



\newpage 

# Analyse en programmation littéraire

### gestion de plusieur pays 
```{r, warning=FALSE}
# Liste des codes des pays sélectionnés
codes_selected_list <- c("MAR", "FRA", "USA", "MEX") # Exemple de codes

# Initialiser une liste pour stocker les data frames combinés de chaque pays
list_df_combined <- list()

for (code_selected in codes_selected_list) {
    df_filtered_tidy_pays <- df_filtered_tidy %>%
      filter(Code == code_selected)

    df_ppa_pays <- ppa_data_tidy %>%
      filter(`Country Code` == code_selected)

    df_ppa_pays_pourcentage <- df_ppa_pays %>%
      arrange(Year) %>%
      mutate(Growth = ((GDP_PPP / lag(GDP_PPP)) - 1) * 100)

    df_sum_energy_pays <- df_filtered_tidy_pays %>%
      group_by(Year) %>%
      summarise(Total_Consumption = sum(Consommation)) %>%
      ungroup()

    df_combined <- merge(df_ppa_pays_pourcentage, df_sum_energy_pays, by = "Year")
    df_combined$Year <- as.numeric(as.character(df_combined$Year))

    # Ajouter le data frame combiné à la liste
    list_df_combined[[code_selected]] <- df_combined
}

```

### Graphique de séries temporelles avec deux axes pour 4 pays
```{r, warning=FALSE}
plots_list <- list()
for (code in names(list_df_combined)) {
  df_combined <- list_df_combined[[code]]
  p <- ggplot(data = df_combined, aes(x = as.numeric(Year))) +
  geom_line(aes(y = Growth, colour = "Croissance du PPA")) + 
  geom_line(aes(y = Total_Consumption / max(Total_Consumption) * max(Growth, na.rm = TRUE), colour = "Consommation d'énergie")) + 
  scale_y_continuous(
    name = "Croissance du PPA (%)",
    sec.axis = sec_axis(~ . / max(df_combined$Growth, na.rm = TRUE) * max(df_combined$Total_Consumption, na.rm = TRUE), name = "Consommation totale d'énergie (TWh)")
  ) +
  theme_minimal() +
  labs(
    title = paste("Corrélation entre PPA et Consommation d'Énergie", code),
    x = "Année"
  ) +
  scale_colour_manual(values = c("Croissance du PPA" = "blue", "Consommation d'énergie" = "red")) +
  theme(legend.position = "bottom") 
  plots_list[[code]] <- p
  print(p)
}


```

#### Description et Conclusion

Ce graphique présente la relation entre le taux de croissance du PPA (Parité de Pouvoir d'Achat) et la consommation totale d'énergie en France. Nous pouvons en tirer les conclusions suivantes :

Tendance à long terme : Sur le long terme, il existe une corrélation positive entre la croissance économique de la France (représentée par le taux de croissance du PPA) et la consommation d'énergie. Cela signifie que l'augmentation des activités économiques est généralement accompagnée d'une demande accrue d'énergie.

Efficacité et transformation : À certaines périodes, comme autour de l'année 2000, même si l'économie a connu une croissance, l'augmentation de la consommation d'énergie n'a pas été significative. Cela pourrait refléter une amélioration de l'efficacité énergétique ou une transition structurelle de l'économie vers des secteurs de services moins dépendants de l'énergie.

Impact des crises économiques : Lors d'événements économiques mondiaux, tels que la crise financière de 2008, à la fois la croissance économique et la consommation d'énergie ont diminué, ce qui montre l'impact direct de l'environnement macroéconomique sur la demande en énergie.

Impact de la pandémie : Ces dernières années, en particulier depuis le début de la pandémie de COVID-19, la consommation d'énergie a considérablement diminué. Cela pourrait être lié au ralentissement économique associé à la pandémie, aux blocages des industries et aux restrictions de voyage.

Politique énergétique et objectifs climatiques : Le graphique montre une tendance récente à la stabilisation, voire à la diminution de la consommation d'énergie, ce qui pourrait également être lié à la politique énergétique de la France et à ses objectifs de réduction des émissions, indiquant des investissements dans les énergies durables et les technologies propres.

### Nuage de Point

```{r, warning=FALSE}
for (code in names(list_df_combined)) {
  df_combined <- list_df_combined[[code]]
  p <- ggplot(df_combined, aes(x = Total_Consumption, y = Growth)) +
    geom_point() +  # Ajoute les points de données
    geom_smooth(method = "lm", se = FALSE, color = "blue") +  # Ajoute une ligne de tendance linéaire
    theme_minimal() +
    labs(
      title = "Relation entre la Croissance du PPA et la Consommation Totale d'Énergie",
      x = "Consommation Totale d'Énergie (TWh)",
      y = "Croissance du PPA (%)"
    ) +
    geom_text(aes(label = Year), check_overlap = TRUE, vjust = -0.5) # Optionnel : ajouter les années pour chaque point
  print(p)
}

```

#### Description et Conclusion

Cette carte est un diagramme de dispersion qui illustre la relation entre le taux de croissance de la PPA (Parité de Pouvoir d'Achat) et la consommation totale d'énergie en France (mesurée en térawattheures, TWh) au cours de différentes années. Chaque point représente une année spécifique, avec la consommation d'énergie sur l'axe des abscisses et le taux de croissance de la PPA sur l'axe des ordonnées.

En analysant ce diagramme, nous pouvons observer que :

- Les points ne forment pas de relation linéaire évidente, ce qui suggère qu'il n'y a pas de corrélation directe et simple entre le taux de croissance de la PPA et la consommation d'énergie.
- La plupart des points sont concentrés entre 1500 et 2000 TWh de consommation d'énergie. Dans cette gamme, le taux de croissance de la PPA varie de valeurs négatives à environ 10%, indiquant une grande volatilité dans le taux de croissance économique.
- Pour les années ayant une consommation d'énergie élevée (proche de 2250 TWh), le taux de croissance de la PPA semble être relativement bas. Cela pourrait indiquer que durant les années de forte consommation d'énergie, l'efficacité de la croissance est faible, ou que la croissance est principalement tirée par des industries à faible efficacité énergétique.
- Pendant certaines années, comme 2009 et 2020, une diminution de la consommation d'énergie et une baisse du taux de croissance de la PPA peuvent être observées, ce qui pourrait être associé à une récession économique, comme la crise financière mondiale et la pandémie de COVID-19.
- Dans les années les plus récentes, comme en 2022, bien que la consommation d'énergie n'ait pas augmenté de manière significative, le taux de croissance de la PPA est resté à un niveau élevé. Cela pourrait refléter une amélioration de l'efficacité énergétique ou une transition vers une structure énergétique plus durable.

Globalement, ce diagramme de dispersion nous indique que la relation entre la croissance économique et la consommation d'énergie en France est complexe et variable en fonction des différents contextes économiques, sociaux et politiques. Pour interpréter précisément chaque point de données annuel, il est nécessaire de prendre en compte les événements spécifiques de cette année, les changements de politique et d'autres facteurs qui peuvent affecter la demande d'énergie et la croissance économique.


\newpage

# Synthèse des Résultats
L'analyse des données sur la production d'électricité par source et le PIB dans les pays sélectionnés révèle plusieurs tendances intéressantes qui soulignent la relation complexe entre le mix énergétique et la croissance économique.

Corrélation entre le Mix Énergétique et la Croissance Économique:

Dans des pays comme la France et les États-Unis, où une portion significative de l'électricité provient de sources à faible émission de carbone (nucléaire et renouvelables), le PIB par habitant est généralement plus élevé. Cela suggère une possible corrélation positive entre un mix énergétique plus propre et une économie plus robuste.
En revanche, l'Afrique du Sud, avec sa forte dépendance au charbon, montre que les économies fortement dépendantes des combustibles fossiles peuvent aussi avoir des économies importantes, mais cela soulève des questions sur la durabilité et les coûts environnementaux à long terme.
Causalité Potentielle:

Bien que ces observations suggèrent une corrélation, la causalité n'est pas clairement établie. Une économie plus forte peut permettre des investissements plus importants dans les technologies d'énergie propre, conduisant à un mix énergétique plus durable. Inversement, un engagement envers l'énergie durable peut stimuler l'innovation et la croissance économique par de nouveaux secteurs industriels et une meilleure efficacité énergétique.
Limites de l'Étude:

Cette analyse est limitée par son champ d'application et la disponibilité des données. La considération d'un plus grand nombre de pays et d'années pourrait fournir des aperçus plus nuancés sur ces relations.
De plus, d'autres facteurs économiques et environnementaux, tels que les politiques gouvernementales, les conditions géographiques, et les prix mondiaux de l'énergie, jouent également un rôle crucial dans la détermination du mix énergétique et de la croissance économique, mais n'ont pas été pris en compte dans cette analyse.
Implications pour la Politique Énergétique et Économique:

Les résultats suggèrent que les pays peuvent bénéficier économiquement en investissant dans des sources d'énergie renouvelables et propres. Une politique énergétique axée sur la durabilité peut non seulement aider à atténuer le changement climatique mais aussi stimuler la croissance économique.
Les gouvernements devraient envisager de soutenir la recherche et le développement dans les technologies d'énergie propre, ainsi que de mettre en place des cadres réglementaires et des incitations pour encourager la transition énergétique.
Conclusion
Notre analyse souligne l'importance d'un mix énergétique durable pour le développement économique. Toutefois, il est crucial de reconnaître la complexité des relations entre énergie et économie et de tenir compte d'une gamme de facteurs lors de l'élaboration des politiques énergétiques et économiques. Une approche holistique, prenant en considération à la fois les impératifs économiques et environnementaux, sera essentielle pour naviguer vers un avenir énergétique durable et prospère.

# References

PPA : https://donnees.banquemondiale.org/indicator/NY.GDP.MKTP.CD
mix-energique : https://ourworldindata.org/energy-mix

