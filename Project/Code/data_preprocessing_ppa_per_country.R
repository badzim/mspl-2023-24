library(tidyverse)

ppa_data <- read_csv("Datasets/Data/Cleaned_API_NY.GDP.MKTP.PP.CD_DS2_fr_csv_v2_12982.csv")
ppa_data <- read_csv("Datasets/Data/Cleaned_API_NY.GDP.MKTP.PP.CD_DS2_fr_csv_v2_12982.csv", show_col_types = FALSE)


problems(ppa_data)

ppa_data_tidy <- ppa_data %>%
  pivot_longer(
    cols = `1990`:`2022`, 
    names_to = "Year", 
    values_to = "GDP_PPP"
  )

print(ppa_data_tidy)

library(dplyr)

# Filtre pour le pays choisi, par exemple "Aruba"
data_pays <- ppa_data_tidy %>%
filter(`Country Name` == "Aruba")

library(dplyr)

data_pays_pourcentage <- data_pays %>%
  arrange(Year) %>% # Assurez-vous que les données sont dans l'ordre chronologique
  mutate(
    Growth = (GDP_PPP / lag(GDP_PPP) - 1) * 100 # Calcule le pourcentage de croissance annuelle
  )



library(ggplot2)

ggplot(data_pays_pourcentage, aes(x = as.numeric(Year), y = Growth)) +
  geom_line() + # Trace une ligne pour connecter les points de croissance
  geom_point() + # Marque chaque année avec un point
  theme_minimal() + # Utilise un thème minimal pour une apparence épurée
  labs(title = "Croissance annuelle du PPA d'Aruba en pourcentage",
       x = "Année",
       y = "Croissance du PPA (%)") +
  theme(plot.title = element_text(hjust = 0.5)) # Centre le titre du graphique

